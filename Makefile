# Copyright (c) 2010 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

SRC ?= src

all:
	$(MAKE) -C $(SRC)

%:
	$(MAKE) -C $(SRC) $@

.PHONY: all
