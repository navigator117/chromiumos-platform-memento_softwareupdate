// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CHROMEOS_PLATFORM_MEMENTO_UPDATER_DISKIMAGE_INSTALLER_H__
#define CHROMEOS_PLATFORM_MEMENTO_UPDATER_DISKIMAGE_INSTALLER_H__

#include "general_installer.h"

#include <stdint.h>

#include <string>

namespace chromeos_memento_updater {

enum DiskImageInstallFormat{
  kImage = 0,
  kZip = 1
};

class DiskImageInstaller : public GeneralInstaller {
 public:
  DiskImageInstaller(const std::string& running_dir,
                     const std::string& board,
                     const std::string& image_path,
                     DiskImageInstallFormat format);
  ~DiskImageInstaller();
  const static int kBlockSize = 512;

 protected:
  FRIEND_TEST(DiskImageInstallerTest, PrepareImage);
  FRIEND_TEST(DiskImageInstallerTest, PrepareZipImage);
  virtual int PrepareImage(const int channel_id, FILE* out_pipe);

  // Read the offset (by number of blocks) of a partition in an image by cgpt
  // Return -1 on error
  int64_t GetImageOffsetInBlock(const std::string& image_path,
                                const int partition);

  // Read the size (by number of blocks) of a partition in an image by cgpt
  // Return -1 on error
  int64_t GetImageSizeInBlock(const std::string& image_path,
                              const int partition);
  int DecompressImage(FILE** fp);
  std::string image_path_;
  DiskImageInstallFormat format_;
};

}  // namespace chromeos_memento_updater

#endif  // CHROMEOS_PLATFORM_MEMENTO_UPDATER_DISKIMAGE_INSTALLER_H__

