// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CHROMEOS_PLATFORM_MEMENTO_UPDATER_ERROR_H__
#define CHROMEOS_PLATFORM_MEMENTO_UPDATER_ERROR_H__

#include <error.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#include "mount_handler.h"
#include "tempfile_handler.h"

namespace chromeos_memento_updater {

enum PartitionNum {
  kStatefulPartition = 1,
  kFactoryKernelPartition = 2,
  kFactoryPartition = 3,
  kReleaseKernelPartition = 4,
  kReleasePartition = 5,
  kOemPartition = 8,
  kEfiPartition = 12
};

inline int error(const std::string& str){
  perror(str.c_str());
  return 1;
}

inline void error_exit(const std::string& str){
  exit(error(str));
}

inline void system_call(const std::string& command,
                        const std::string& error_msg){
  if (system(command.c_str()) != 0) {
    error_exit(error_msg);
  }
}

}  // namespace chromeos_memento_updater

#endif  // CHROMEOS_PLATFORM_MEMENTO_UPDATER_ERROR_H__

