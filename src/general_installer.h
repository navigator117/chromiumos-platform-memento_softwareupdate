// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CHROMEOS_PLATFORM_MEMENTO_UPDATER_GENERAL_INSTALLER_H__
#define CHROMEOS_PLATFORM_MEMENTO_UPDATER_GENERAL_INSTALLER_H__

#include <gtest/gtest_prod.h>
#include <string>

namespace chromeos_memento_updater {

class GeneralInstaller {
 public:
  GeneralInstaller(const std::string& running_dir,
                   const std::string& board);
  virtual ~GeneralInstaller();
  virtual void ResetPartitionTable(const std::string& device_path);
  virtual void InitStatefulPartition(const std::string& device_path);
  // Install device by reading/writing simultaneously (with fork).
  virtual void InstallDevice(const int channel_id,
                             const std::string& device_path,
                             const std::string& kernel_device_path);
  virtual void ZeroOutDevice(const std::string& device_path);
  // When firmware path are not given, here we use the firmware updater on
  // current rootfs. Override this to obtain firmware updater in other ways.
  virtual std::string PrepareFirmware(const std::string& option);
  virtual void UpdateFirmware(const std::string& updater_path,
                              const std::string& mode);
  virtual void ActivateImage(const std::string& device_path);
  bool CombinedKernel() { return combined_kernel_; }
  bool AllowPartitionOptions() { return allow_partition_options_; }
 protected:
  FRIEND_TEST(GeneralInstallerTest,InstallSingleImage);
  FRIEND_TEST(GeneralInstallerTest,InstallTwoImages);
  virtual int PrepareImage(const int channel_id,
                           FILE* out_pipe);
  virtual int InstallImage(const std::string& device_path,
                           const std::string& kernel_device_path,
                           FILE* in_pipe);
  // There is no general prepare image method.
  bool IsDeviceMounted(const std::string& device_path);
  std::string running_dir_;
  std::string board_;
  bool combined_kernel_;
  bool allow_partition_options_;
};

}  // namespace chromeos_memento_updater

#endif  // CHROMEOS_PLATFORM_MEMENTO_UPDATER_GENERAL_INSTALLER_H__
