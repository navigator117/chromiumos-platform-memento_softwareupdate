// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CHROMEOS_PLATFORM_MEMENTO_UPDATER_COMMAND_HANDLER_H__
#define CHROMEOS_PLATFORM_MEMENTO_UPDATER_COMMAND_HANDLER_H__

#include <string>
#include <utility>
#include <vector>

#include "general_installer.h"
#include "memento_common.h"

namespace chromeos_memento_updater {

class CommandHandler {
 public:
  enum InstallOption {
    kResetDevice = 0,
    kResetPartition,
    kFactory,
    kRelease,
    kStateful,
    kOem,
    kEfi,
    kFirmware,
    kActivate
  };
  const static std::string kOptionList[];
  const static int kOptions;

  CommandHandler(int argc, char** argv);
  ~CommandHandler();
  std::string GetInputDevice() { return input_device_; }
  std::string GetOutputDevice() { return output_device_; }
  std::string GetOutputDevicePartition(PartitionNum partition);
  std::vector< std::pair<InstallOption, std::string> > GetOptionList() {
    return option_list_;
  }
  GeneralInstaller* GetInstallerPtr() { return installer_ptr_; }
  bool ReleaseOnly() { return have_release_ && !have_factory_; }
  bool FactoryOnly() { return !have_release_ && have_factory_; }
 private:
  // We assume the number of elements in InstallOption and kOptionList
  // equal to kOptions, and the enum is assumed to be start from zero
  // and increment by 1.
  // The options are gauranteed to be executed in order.
  void Usage();
  void ParseCommandLine(int argc, char** argv);
  void ParseInputDevice(const std::string& input_string,
                        const std::string& running_dir);
  void ParseContent(const std::string& content_string);
  void SetRootDev();
  std::string input_device_;
  std::string output_device_;
  std::string root_device_;
  std::string board_;

  // These bools are used for auto detect device to install.
  bool have_release_;
  bool have_factory_;

  std::vector< std::pair<InstallOption, std::string> > option_list_;
  GeneralInstaller* installer_ptr_;
};

}  // namespace chromeos_memento_updater

#endif  // CHROMEOS_PLATFORM_MEMENTO_UPDATER_COMMAND_HANDLER_H__
