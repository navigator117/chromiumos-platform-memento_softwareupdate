// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CHROMEOS_PLATFORM_MEMENTO_UPDATER_MOUNT_HANDLER_H__
#define CHROMEOS_PLATFORM_MEMENTO_UPDATER_MOUNT_HANDLER_H__

#include <string>
#include <sys/mount.h>

namespace chromeos_memento_updater {

class MountHandler {
 public:
  MountHandler(const std::string& device_path,
               const std::string& file_system,
               unsigned long mount_flags);
  ~MountHandler();

  std::string GetMountPoint();

 private:
  std::string mount_point_;
};

}  // namespace chromeos_memento_updater

#endif  // CHROMEOS_PLATFORM_MEMENTO_UPDATER_MOUNT_HANDLER_H__
