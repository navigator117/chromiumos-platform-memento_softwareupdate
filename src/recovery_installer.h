// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CHROMEOS_PLATFORM_MEMENTO_UPDATER_RECOVERY_INSTALLER_H__
#define CHROMEOS_PLATFORM_MEMENTO_UPDATER_RECOVERY_INSTALLER_H__

#include "general_installer.h"
#include "diskimage_installer.h"

#include <stdint.h>

#include <string>

namespace chromeos_memento_updater {

class RecoveryInstaller : public DiskImageInstaller {
 public:
  RecoveryInstaller(const std::string& running_dir,
                    const std::string& board);
  // In case that image is too large, here we let user to assign
  // the directory to put image.
  RecoveryInstaller(const std::string& running_dir,
                    const std::string& board,
                    const std::string& image_dir);
  ~RecoveryInstaller();

  // Override for post processing release
  virtual void InstallDevice(const int channel_id,
                             const std::string& device_path,
                             const std::string& kernel_device_path);
 protected:
  FRIEND_TEST(RecoveryInstallerTest, DownloadImage);
  // Used to find url from the config file
  bool FindKeyValue(const std::string& line, const std::string& key,
                    std::string& value);

  void DownloadImageFromServer();
  const static std::string kConfigUrl;
  const static std::string kConfigPath;
  const static std::string kZipImagePath;
  std::string config_path_;
  std::string image_dir_;
};

}  // namespace chromeos_memento_updater

#endif  // CHROMEOS_PLATFORM_MEMENTO_UPDATER_RECOVERY_INSTALLER_H__

