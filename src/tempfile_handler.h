// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef CHROMEOS_PLATFORM_MEMENTO_UPDATER_TEMPFILE_HANDLER_H__
#define CHROMEOS_PLATFORM_MEMENTO_UPDATER_TEMPFILE_HANDLER_H__

#include <string>

namespace chromeos_memento_updater {

class TempfileHandler {
 public:
  TempfileHandler(bool auto_delete = true);
  ~TempfileHandler();
  std::string GetFilePath();
 protected:
  std::string file_path_;
  bool auto_delete_;
};

}  // namespace chromeos_memento_updater

#endif  // CHROMEOS_PLATFORM_MEMENTO_UPDATER_TEMPFILE_HANDLER_H__
