// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "memento_common.h"
#include "tempfile_handler.h"

#include <stdio.h>
#include <stdlib.h>

using std::string;

namespace chromeos_memento_updater {

TempfileHandler::TempfileHandler(bool auto_delete) {
  char file_template[] = "/tmp/mmt_XXXXXX";
  auto_delete_ = auto_delete;
  file_path_.assign(mktemp(file_template));
  if (file_path_.empty()) {
    error_exit("Failed to create tempfile");
  }
}

string TempfileHandler::GetFilePath() {
  return file_path_;
}

TempfileHandler::~TempfileHandler() {
  if (auto_delete_ && remove(file_path_.c_str()) != 0) {
    error("Cannot remove tempfile " + file_path_);
  }
}

}  // namespace chromeos_memento_updater

