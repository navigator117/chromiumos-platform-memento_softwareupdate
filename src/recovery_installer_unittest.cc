// Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "memento_common.h"
#include "general_installer.h"
#include "recovery_installer.h"
#include <gtest/gtest.h>

#include <stdlib.h>
#include <stdio.h>

#include <string>
#include <sstream>

using std::string;
using std::stringstream;

namespace chromeos_memento_updater {

const string kProgramDir = ".";
const string kTestString = "DiskImageInstallerTest Passed!\n";

class RecoveryInstallerTest : public ::testing::Test {
 protected:
  RecoveryInstallerTest() {}
  ~RecoveryInstallerTest() {}
};


TEST_F(RecoveryInstallerTest, DownloadImage) {
  // TODO(chunyen): Can we test with a fake server?
  RecoveryInstaller installer(kProgramDir, "x86-alex-he", kProgramDir);
  FILE* fp = fopen(installer.image_path_.c_str(), "r");
  EXPECT_TRUE(fp != NULL);
  fclose(fp);
}

// TODO(chunyen): Add unittest for other functions.

}

int main(int argc, char** argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

